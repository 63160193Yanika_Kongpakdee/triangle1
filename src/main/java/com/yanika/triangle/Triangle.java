/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yanika.triangle;

/**
 *
 * @author WINDOWS10
 */
public class Triangle {
    private double b;
    private double h;
    public static final double Half = 1.0/2;
    public Triangle(double b,double h){
        this.b = b;
        this.h = h;
    }public double calArea(){
        return Half * b *h;
    } double getB(){
        return b;
    }public double getH(){
        return h;
    }public void setBH(double b,double h){
        if(b<=0) {
            System.out.println("Error: base must more than zero!!!");
            return;
        }if(h<=0) {
            System.out.println("Error: height must more than zero!!!");
            return;
        }
        this.b = b;
        this.h = h;
    }



}
